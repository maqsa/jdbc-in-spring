import org.springframework.context.support.GenericXmlApplicationContext;
import java.util.List;

public class JdbcContactDaoSample {
    private static void listContacts(List<Contact> contacts){
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetailList()!=null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetailList())
                    System.out.println("---" + contactTelDetail);
            }
        }
        System.out.println();

    }
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/app-context-xml.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);


        System.out.println();
        /*Извлечение вложенных объектов предметной области с помощью ResultSetExtractor */
        List<Contact> contactsWithDetail = contactDao.findAllWithDetail();
        System.out.println("___ > Извлечение вложенных объектов предметной области с помощью ResultSetExtractor");
        for (Contact contact: contactsWithDetail){
            System.out.println(contact);

            if (contact.getContactTelDetailList() != null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetailList()){
                    System.out.println("---"  + contactTelDetail);
                }
            }
            System.out.println();
        }



        /*Извлечение объектов предметной области с помощью RowМapper<T>*/
        System.out.println("___ > Извлечение объектов предметной области с помощью RowМapper<T>");
        List<Contact> contacts = contactDao.findAll();

        for (Contact contact: contacts) {
            System.out.println(contact);

            if (contact.getContactTelDetailList() != null) {
                for (ContactTelDetail contactTelDetail:
                        contact.getContactTelDetailList()) {
                    System.out.println("---" + contactTelDetail);
                }
            }

            System.out.println();
        }

        /*Использование именованных параметров с NamedParameterJdЬcTemplate */
        System.out.println("___ > Использование именованных параметров с NamedParameterJdЬcTemplate");
        System.out.println("Last name for contact id 1 is: " + contactDao.findLastNameById(1l));
        System.out.println();
        /*Извлечение одиночного значения с использованием класса JdbcTemplate */
        System.out.println("___ > Извлечение одиночного значения с использованием класса JdbcTemplate");
        System.out.println("First name for contact id 1 is: " +
                contactDao.findFirstNameById(1l));
    }
}
