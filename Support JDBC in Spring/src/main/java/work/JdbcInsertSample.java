package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class JdbcInsertSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-xml.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);

        /*Вставка данных и извлечение сгенерированного ключа */

        Contact contact = new Contact();
        contact.setFirstName("Rody");
        contact.setLastName("Johneson");
        contact.setBirthDate(new Date(
                (new GregorianCalendar(2000, 10, 1).getTime().getTime())
        ));
        contactDao.insert(contact);
        listContacts(contactDao.findAll());
    }

    private static void listContacts(List<Contact> contacts){
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetailList()!=null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetailList())
                    System.out.println("---" + contactTelDetail);
            }
        }
        System.out.println();

    }
}
