package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class JdbcUpdateSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-xml.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);

        /*Обновление данных с использованием SqlUpda te */
        Contact contact = new Contact();
        contact.setId(1l);
        contact.setFirstName("Chris");
        contact.setLastName("John");
        contact.setBirthDate(new Date(
                (new GregorianCalendar(1972, 10, 1).getTime().getTime())
        ));
        contactDao.update(contact);
        listContacts(contactDao.findAll());
    }

    private static void listContacts(List<Contact> contacts){
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetailList()!=null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetailList())
                    System.out.println("---" + contactTelDetail);
            }
        }
        System.out.println();

    }
}
