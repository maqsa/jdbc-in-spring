package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.sql.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class JdbcBatchSqlUpdateSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-xml.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);
        /*Объединение операций в пакеты с помощью ВаtchSqlUpdate */

        Contact contact = new Contact();
        contact.setFirstName("Michaele");
        contact.setLastName("Jacksone");
        contact.setBirthDate(new Date(
                (new GregorianCalendar(1964, 10, 1).getTime().getTime())
        ));
        List<ContactTelDetail> contactTelDetails = new ArrayList<ContactTelDetail>();
        ContactTelDetail contactTelDetail = new ContactTelDetail();
        contactTelDetail.setTelType("Home");
        contactTelDetail.setTelNumber("2222");
        contactTelDetails.add(contactTelDetail);
        contactTelDetail = new ContactTelDetail();
        contactTelDetail.setTelType("Mobile");
        contactTelDetail.setTelNumber("2222222222");

        contactTelDetails.add(contactTelDetail);
        contact.setContactTelDetailList(contactTelDetails);
        contactDao.insertWithDetail(contact);
        listContacts(contactDao.findAllWithDetail());
    }

    private static void listContacts(List<Contact> contacts){
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetailList()!=null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetailList())
                    System.out.println("---" + contactTelDetail);
            }
        }
        System.out.println();

    }
}
