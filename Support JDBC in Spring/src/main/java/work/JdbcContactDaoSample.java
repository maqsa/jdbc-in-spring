package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.sql.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class JdbcContactDaoSample {
    private static void listContacts(List<Contact>contacts){
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetailList()!=null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetailList())
                System.out.println("---" + contactTelDetail);
            }
        }
        System.out.println();

    }
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-xml.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);

        /*Вывод хранимых функций с используванием SqlFunction*/
        System.out.println("___ > Вывод хранимых функций с используванием SqlFunction");
        System.out.println(contactDao.findFirstNameById(1l));


      /*Запрашивание данных с использованием МappingSqlQuery<T> */
        System.out.println("___ > Запрашивание данных с использованием МappingSqlQuery<T>");
        List<Contact> contacts = contactDao.findByFirstName("Chris");
        listContacts(contacts);

    }
}
